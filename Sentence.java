package myPackage;

public class Sentence implements SentenceInterface {
	private static String sentencePrimary;
	
	@Override
	public void setSentencePrimary(String sentence) {
		Sentence.sentencePrimary = sentence;
	}

	@Override
	public String[] stringToWords() {
		String sentence = sentencePrimary;
		String[] words = sentence.split(" ");
		return words;
	}
}
