package myPackage;

public interface WordInterface {
	void setNumber(int number);
	void setSymbol(char symbol);
	String proceedWords(String[] words);
}
