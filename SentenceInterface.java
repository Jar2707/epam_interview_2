package myPackage;


public interface SentenceInterface {
	void setSentencePrimary(String sentence);
	public String[] stringToWords();
}
