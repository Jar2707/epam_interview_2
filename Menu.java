package myPackage;

import java.util.Scanner;

public class Menu {
	private String menu0Intro = "This program replace each k-th symbol in each word of the text.\n"
			+ "You must select number of the letter to change and symbol to which"
			+ "the letter will be replaced";
	private String menu1Intro = "Select sentence source (enter 1-3):\n"
	        + "1. Console input;\n"
	        + "2. Absolute way to the file;\n"
	        + "3. Text by default(\"aaaaaa! bbbbb, cccc. ddd? ee: f; g\")";
    private String menu1_1Intro = "Enter sentence: ";
    private String menu1_2Intro = "Enter absolute way to the file: ";
    private String menu2ChooseNumber = "Enter a number of a symbol to change: ";
    private String menu3ChooseSymbol = "Enter a symbol to which You want to replace: ";
    private String menu4Completion = "Select option:\n"
            + "1. Print resulting sentence to the console;\n"
            + "2. Output resulting sentence to the file;\n"
            + "3. Restart the program;\n"
            + "4. Exit the program;";
    private String menu1IntroIncorrect = "Please, enter 1, 2 or 3: ";
    private String menu2ChooseNumberIncorrect = "Please, enter a non-negative integer: ";
    private String menu4CompletionIncorrect = "Please, enter the number from 1 to 4: ";
    
    private void print0Intro() {
    	System.out.println(menu0Intro);
    }
    private void print1Intro() {
        System.out.println(menu1Intro);
    }
    private void print1_1Intro() {
    	System.out.println(menu1_1Intro);
    }
    private void print1_2Intro() {
    	System.out.println(menu1_2Intro);
    }
    private void print1IntroIncorrect() {
        System.out.println(menu1IntroIncorrect);
    }
    private void print2ChooseNumber() {
        System.out.println(menu2ChooseNumber);
    }
    private void print2ChooseNumberIncorrect() {
        System.out.println(menu2ChooseNumberIncorrect);
    }
    private void print3ChooseSymbol() {
        System.out.println(menu3ChooseSymbol);
    }
    private void print4Completion() {
        System.out.println(menu4Completion);
    }
    private void print4CompletionIncorrect() {
        System.out.println(menu4CompletionIncorrect);
    }
    
    private int pickUp1Intro() {
    	int choice = -1;
    	Scanner sc = new Scanner(System.in);
    	while(!(choice >= 1 && choice <= 3)) {
    		if(sc.hasNextInt()) {
    			choice = sc.nextInt();
    			if(choice < 1 || choice > 3) {
    				print1IntroIncorrect();
    				sc.nextLine();
    			}
    		} else {
    			print1IntroIncorrect();
    			sc.nextLine();
    		}
    	}
    	return choice;
    }
    
    private String pickUp1_1Intro() {
    	String sentence = "";
    	Scanner sc = new Scanner(System.in);
    	sentence = sc.nextLine();
    	return sentence;
    }
    
    private String pickUpFileLocation() {
    	String fileLocation = "";
    	Scanner sc = new Scanner(System.in);
    	fileLocation = sc.nextLine();
    	return fileLocation;
    }
    
    private int pickUp2ChooseNumber() {
    	int choice = -1;
    	Scanner sc = new Scanner(System.in);
    	while(choice < 0) {
    		if(sc.hasNextInt()) {
    			choice = sc.nextInt();
    			if(choice < 0) {
    				print2ChooseNumberIncorrect();
    				sc.nextLine();
    			}
    		} else {
    			print2ChooseNumberIncorrect();
    			sc.nextLine();
    		}
    	}
    	return choice;
    }
    
    private char pickUp3ChooseSymbol() {
    	char choice;
    	Scanner sc = new Scanner(System.in);
    	String stringChoice = sc.nextLine();
    	char charChoice = stringChoice.charAt(0);
    	return charChoice;
    }
    
    private int pickUp4Completion() {
    	int choice = -1;
    	Scanner sc = new Scanner(System.in);
    	while(!(choice >= 1 && choice <= 4)) {
    		if(sc.hasNextInt()) {
    			choice = sc.nextInt();
    			sc.nextLine();
    			if(choice < 1 || choice > 4) {
    				print4CompletionIncorrect();
    			}
    		} else {
    			print4CompletionIncorrect();
    		}
    	}
    	return choice;
    }
    
    public void block() {
    	FileWorker fw = new FileWorker();
    	Sentence sentence = new Sentence();
    	Word word = new Word();
    	int choice1;
    	int choice2 = -1;
    	print0Intro();
       	while(choice2 != 4) {
        	choice2 = -1;
    		print1Intro();
    		choice1 = pickUp1Intro();
    		switch (choice1) {
    		case 1:
    			print1_1Intro();
    			fw.readTextFromConsole(pickUp1_1Intro());
    			break;
    		case 2:
    			print1_2Intro();
    			fw.setInputFileLocation(pickUpFileLocation());
    			fw.readTextFromTheFile();
    			break;
    		case 3:
    			fw.setTextByDefault();
    			break;
    		default:
    			break;
    		}
    		print2ChooseNumber();
    		word.setNumber(pickUp2ChooseNumber());
    		print3ChooseSymbol();
    		word.setSymbol(pickUp3ChooseSymbol());
    		fw.proceedText();
    		while(!(choice2 == 3 || choice2 == 4)) {
    			print4Completion();
    			choice2 = pickUp4Completion();
    			switch (choice2) {
    			case 1:
    				fw.writeTextToTheConsole();
    				break;
    			case 2:
    				print1_2Intro();
    				fw.setOutputFileLocation(pickUpFileLocation());
    				fw.writeTextToTheFile();
    				break;
    			case 3:
    				break;
    			case 4:
    				break;
    			default:
    				break;
    			}
    		}	
    	}    
    	System.out.println("Program terminated!");
    }
}
