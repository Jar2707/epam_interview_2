package myPackage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class FileWorker {
	private static String inputFileLocation;
	private static String outputFileLocation;
	private static ArrayList<String> textPrimary;
	private static ArrayList<String> textModified;
	
	public void setInputFileLocation(String s) {
		this.inputFileLocation = s;
	}
	public void setOutputFileLocation(String s) {
		this.outputFileLocation = s;
	}
	public void proceedText() {
		Sentence sentence = new Sentence();
		Word word = new Word();
		textModified = new ArrayList<String>();
		for(String line : textPrimary) {
			sentence.setSentencePrimary(line);
			textModified.add(word.proceedWords(sentence.stringToWords()));
		}
	}
	public void readTextFromTheFile() {
    	try {
    		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputFileLocation), Charset.forName("UTF-8")));
			//BufferedReader reader = new BufferedReader(new FileReader(inputFileLocation));
			String line;
			textPrimary = new ArrayList<String>();
			while((line = reader.readLine()) != null) {
				textPrimary.add(line);
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    public void writeTextToTheFile() {
    	try{
    		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFileLocation), Charset.forName("UTF-8")));  
    		for(String line : textModified) {
    			if(line != null) {
    				writer.write(line);
    				writer.append("\r\n");
    			}
    		}
    		writer.close();
    	}
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void readTextFromConsole(String s) {
    	textPrimary = new ArrayList<String>();
    	textPrimary.add(s);
    }
    public void writeTextToTheConsole() {
    	for(String line : textModified) {
    		System.out.println(line);
    	}
    }
    public void setTextByDefault() {
    	textPrimary = new ArrayList<String>();
    	textPrimary.add("aaaaaa! bbbbb, cccc. ddd? ee: f; g");
    }
}
